package musiktest;

import com.jsyn.*;
import com.jsyn.unitgen.FilterStateVariable;
import com.jsyn.unitgen.LineOut;
import com.jsyn.unitgen.WhiteNoise;


public class Main {

    public static void main(String[] args) {
        // Initialisieren
        Synthesizer synth = JSyn.createSynthesizer();
        synth.start( 8000 );

        // Unit Generators erzeugen
        LineOut myOut = new LineOut();
        WhiteNoise myNoise = new WhiteNoise();
        FilterStateVariable myFilter = new FilterStateVariable();

        synth.add( myOut );
        synth.add( myNoise );
        synth.add( myFilter);

        // Generatoren verbinden
        myNoise.output.connect( myFilter.input );

        myFilter.output.connect( 0, myOut.input, 0 ); /* Left side */
        myFilter.output.connect( 0, myOut.input, 1 ); /* Right side */

        myOut.start();
    }
}
